/**
 * Created by ak on 10/23/2016.
 */
/// <reference path="Script.js"/>

myApp.controller("sitesController", function ($scope, $http, $window, $log) {
    $scope.baseresponse = {};
    $scope.sites = {};
    $scope.base = {};
    $scope.base = JSON.parse($window.localStorage.getItem('entity'));
    $scope.id = $scope.base.response.id;
    $scope.details = {};
    // Posting data to  rest web api
    $http({
        method: 'GET',
        url: 'http://localhost:7777/tracker/' + $scope.id
    }).then(function success(response) {
        console.log("Hello");
        console.log(response.data);
        $scope.baseresponse = response.data;
        $scope.sites = $scope.baseresponse.response;
        console.log($scope.sites);
    }, function error(r) {
        console.log(r);
    });


});
