/**
 * Created by ak on 10/23/2016.
 */
/// <reference path="Script.js"/>
myApp.controller("registrationController", function ($scope, $http, $window, $log) {
    // create a blank object to handle form data.
    $scope.user = {};
    $scope.user.id = 0;
    $scope.user.sites = [];
    // calling our submit function.
    $scope.submitForm = function () {
        // Posting data to  rest web api
        $http({
            method: 'POST',
            url: 'http://localhost:7777/users',
            data: $scope.user//forms user obj
        })
            .then(function (response) {

                $window.localStorage.token = response.headers('Authorization');
                console.log($window.localStorage.token);

                $window.localStorage.setItem('entity', JSON.stringify(response.data)); //storing json object into localstorage
                $window.location.href = "home.html";
            })

    };
    $scope.msg = "registration controller loaded";
});
