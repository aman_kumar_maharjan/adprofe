/// <reference path="Script.js"/>

myApp.controller("logOutController", function ($scope, $http, $window, $log) {
    $scope.base = JSON.parse($window.localStorage.getItem('entity'));
    $scope.id = $scope.base.response.id;
    $scope.user = $scope.base.response;
    $scope.logOut = function () {
        $http({
            method: 'POST',
            url: 'http://localhost:7777/logout/' + $scope.id,
        }).then(function (response) {
            $window.localStorage.removeItem('entity');
            $window.location.href = "index.html";
        });
    };
});