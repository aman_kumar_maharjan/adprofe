/// <reference path="Script.js"/>

myApp.controller("loginController", function ($scope, $http, $window, $log) {
    $scope.credential = {};
    $scope.credential.id = 0;
    // calling our submit function.
    $scope.submitFormLogin = function () {
        // Posting data to  rest web api
        $http({
            method: 'POST',
            url: 'http://localhost:7777/login',
            data: $scope.credential, //form data
        }).then(function (response) {

            $window.localStorage.token = response.headers('Authorization');
            console.log(response.headers('Authorization'));
            console.log($window.localStorage.token);

            $window.localStorage.setItem('entity', JSON.stringify(response.data)); //storing json object into localstorage
            $window.location.href = "home.html";
        })
    };
    $scope.msg = "login controlller loaded";
});