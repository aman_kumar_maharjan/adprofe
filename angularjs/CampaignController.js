/**
 * Created by ak on 11/9/2016.
 */
/// <reference path="Script.js"/>
myApp.controller("campaignController", function ($scope, $http, $window, $log) {
    $scope.base = {};
    $scope.baseresponse = {};
    $scope.base = JSON.parse($window.localStorage.getItem('entity'));
    $scope.user = $scope.base.response;
    $scope.campaign={};
    $scope.load = function () {
        // do your $() stuff here

        var fieldset, currentFieldset, nextFieldset, prevFieldset;
        //getting all fieldset elements
        fieldset = document.getElementsByTagName('fieldset');
        currentFieldset = fieldset[0];
        console.log("displaying 1st node");
        console.log(currentFieldset);
        //displaying first node
        $(currentFieldset).show();


        /* next button */
        $(".next").click(function () {
            //fieldset
            console.log("inside  button click");
            currentFieldset = $(this).parent();
            console.log("displaying current parent node index");
            console.log(currentFieldset.index());
            $(fieldset[currentFieldset.index()]).hide();
            nextFieldset = $(currentFieldset).next();
            $(nextFieldset).show();
            //progressbar
            $("#progressBar ul li ").eq(nextFieldset.index()).addClass("active");
            /* validation code here */

        });
        /*back button */
        $(".back").click(function () {
            //fieldset
            console.log("inside  button click");
            currentFieldset = $(this).parent();
            console.log("displaying current parent node index");
            console.log(currentFieldset.index());
            $(fieldset[currentFieldset.index()]).hide();
            prevFieldset = $(currentFieldset).prev();
            $(prevFieldset).show();
            //progressbar
            $("#progressBar ul li ").eq(currentFieldset.index()).removeClass("active");

        });

        // alert("dom loaded");
        $(".boxes ul li ").click(function () {
            var boxid = $(this).attr('id');
            console.log(boxid);
            //clearing previously selected boxes for mutual exclusion ..only one box selection
            clearSelectedBoxes(boxid);
            //toggling the  selection and unselection
            if ($(this).hasClass("selected")) {
                $(this).removeClass("selected");
            }
            else {
                $(this).addClass("selected");
            }
        });
        function clearSelectedBoxes(boxid) {
            $(".boxes ul li").each(function () {
                if (!($(this).attr('id') == boxid))
                    $(this).removeClass("selected");
            });
        }

        /* date picker*/
        $(".datepicker").datepicker();



    };

    //don't forget to call the load function
    $scope.load();

});
