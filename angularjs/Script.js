/**
 * Created by ak on 10/2/2016.
 */
/// <reference path="angular.min.js"/>
var myApp = angular.module("adproModule", ['ngRoute'])
    .config(function ($routeProvider, $locationProvider, $httpProvider) {
        $httpProvider.interceptors.push('authInterceptor');
        $routeProvider
            .when("/userhome", {
                templateUrl: "templates/userHome.html",
                controller: "homeController",


            })
            .when("/user", {
                templateUrl: "templates/user.html",
                controller: "userController"
            })
            .when("/sites", {
                templateUrl: "templates/sites.html",
                controller: "sitesController"
            })
            .when("/visitors", {
                templateUrl: "templates/visitor.html",
                controller: "visitorController"
            })
            .when("/ads", {
                templateUrl: "templates/ads.html",
                controller: "adsController"
            })
            .when("/audience", {
                templateUrl: "templates/audience.html",
                controller: "audienceController"
            })
            .when("/campaign", {
                templateUrl: "templates/campaign.html",
                controller: "campaignController",

            })
            .otherwise({
                redirectTo: "/userhome"
            })

    });
myApp.factory('authInterceptor', authInterceptor);
function authInterceptor($window) {
    return {
        request: function (config) {
            if ($window.localStorage.token) {
                config.headers.Authorization = $window.localStorage.token;
            }
            return config;
        }
    }
}


